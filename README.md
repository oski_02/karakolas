# Karakolas notes
This repo is to keep track of progress in Karakolas
* Componentes: 
    | Nombre         | Roles | Email               | telefono  |
    | -------------- | ----- | ------------------- | --------- |
    | Pablo Angulo   |       | pang@cancamusa.net  | 633764993 |
    | Inma           |       | isamcaba@riseup.net |           |
    | Carlos         |       |                     |           |
    | Kazbayadum     |       |                     |           |
    | Info Karakolas |       | info@karakolas.org  |           |
    | Oscar Manrique |       | oscar@easier.tech   |           |
* Technologies:
    * [**Savannah**](https://savannah.nongnu.org/)
    *Software forge for people committed to [free software](https://www.gnu.org/philosophy/free-sw.html)*. 
    [Savannah guide](https://savannah.nongnu.org/userguide/)

    * [mercurial](http://hgbook.red-bean.com/): Source Control Management system designed for easy and efficient handling of very large distributed projects.

    * [Taiga](https://taiga.io/): Project management platform for agile developers.
    * [tpv]: El Terminal Punto de Venta (TPV) es un dispositivo usado en establecimientos comerciales para realizar gestiones de venta. Permite, entre otras cosas, realizar cobros con tarjeta de crédito o débito, imprimir tickets y controlar el inventario. Además, los continuos avances tecnológicos permiten que las opciones para los comerciantes de pequeños y grandes negocios sean cada vez más numerosas.
## Doc Karakolas
Ahora mismo las personas más activas en el desarrollo son Inma y Carlos (que están en copia). Kazbayadum, un compa de Bilbao, está documentando el entorno de desarrollo. Hay otra gente activa en el proyecto, pero no están escribiendo código. Yo he estado quedando con Inma y Carlos un poco estos meses y aspiramos a poder lanzar versión antes de que empiece el segundo semestre (Julio 2018). Nuestra prioridad es hacer algunos cambios en la lógica de los **pedidos coordinados**, y si podemos nos gustaría conectar karakolas con un **TPV virtual** para el proyecto de **compras colectivas**. Para estas cosas no estamos tocando mucho el javascript, pero podríamos plantearlo a partir de Enero, cuando Inma y Carlos tendrán que ocuparse de otros proyectos.

Usamos **bootstrap** y **jquery** de forma esencial, varias librerías
utilitarias que hacen tareas concretas (y en general las hacen bien):
- [corefive filemanager](http://labs.corefive.com/projects/filemanager/)
- [hand-on-table](http://handsontable.com/)
- [ckeditor](http://ckeditor.com/)
- [esprima.js](http://esprima.org/) para compilar javascript a un **AST**
- [jquery form](http://jquery.malsup.com/form/)
- [jqplot](http://www.jqplot.com/)

y otras librerías de las que creo que podríamos prescindir a medio plazo
(son experimentos que quedaron a medias, y dudo si la utilidad que
aportan compensa la complejidad que añaden):

- [ractive](http://ractivejs.org/)
- [bower](http://bower.io/)
- [gulp](http://gulpjs.com/)
- [webpack](http://webpack.github.io/)
- [page.js](https://github.com/visionmedia/page.js)

## First meeting 28/01/2018
- La próxima tarea en la lista de karakolas es **integración de pagos** en **faircoin**.

- También está planteado conectar karakolas con un **tpv**, precisamente para los pedidos de la red de compras colectivas.

- En algún momento nos planteamos hacer **tests de integración** para detectar regresiones en karakolas. Sólo llegué a hacer unas pocas comprobaciones usando python (unittest, requests y beatifulsoup), seguro que se puede hacer mejor. No sé si es prioritario, pero lo bueno de esta tarea es que es relativamente independiente del grueso del código de karakolas.

- Pero mejor incluso, os voy a abrir cuentas en nuestro gestor de proyectos: [taiga.karakolas.org](https://taiga.karakolas.org/). Allí podéis elegir las historias de usuario que más os gusten y/o os resulten más asequibles, o podéis incluir historias nuevas. 
- Por cierto, ¿podéis abrir una cuenta en [savannah](https://savannah.nongnu.org)? no corre prisa pero es necesario para contribuir código.